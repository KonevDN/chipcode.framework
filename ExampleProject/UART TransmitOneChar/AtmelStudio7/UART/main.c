/*
 * UART.c
 *
 * Created: 22/07/2018 13:24:41
 * Author : Engineer
 */ 

#define F_CPU 8000000UL
#define SetBit(x,y) (x|=(1<<y))
#define ClearBit(x,y) (x&=~(1<<y))
#define FlipBit(x,y) (x^=(1<<y))
#define isSetBit(x,y) (x&(1<<y))
#define byte char

#define FOSC 16000000// Clock Speed
#define BAUD 9600
#define MYUBRR FOSC/16/BAUD-1

#include <avr/io.h>
#include <util/delay.h>


void USART_Initialization(void)
{
	// ����� �������� �����
	unsigned int boud = 51;
	UBRR0H = (unsigned byte) (boud >> 8);
	UBRR0L = (unsigned byte) boud;
	unsigned long int b2 = (long)9600*16;
	
	SetBit(UCSR0B,TXEN0); // ���������� ������ �����������
	SetBit(UCSR0C,UCSZ00); // 8 ��� ������ ������
	SetBit(UCSR0C,UCSZ01); // 8 ��� ������ ������
	
	//ClearBit(UCSR0A,MPCM0); // �������� ����������������� �����
	//ClearBit(UCSR0C,UMSEL0); // ������� ���������� �����
	//ClearBit(UCSR0C,UPM01); // �������� ����� ��������
	//ClearBit(UCSR0C,UPM00); // �������� ����� ��������
	//ClearBit(UCSR0C,USBS0); // 1 ����-��� � ������� �������
	//ClearBit(UCSR0B,UCSZ02); // 8 ��� ������ ������ 
	// ������� ������� ����������� ��������� �����
	// ClearBit(UCSR0A,U2X);
}
	
void USART_TransmitData (unsigned char data)
{
	while(!isSetBit(UCSR0A,UDRE) ) {};
	UDR0 = data;
}


int main(void)
{
    /* Replace with your application code */

	USART_Initialization();
    while (1) 
    {
		if (isSetBit(UCSR0A,UDRE0) )
		{
			UDR0 = 'Z';
		}
		_delay_ms(1000);
    }
}

