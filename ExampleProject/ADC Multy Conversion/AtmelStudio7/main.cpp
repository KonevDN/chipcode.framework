/*
 * Created: 26/07/2018 23:21:24
 * Author : Engineer
 */

#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>

#include "Chipcode.Framework/LifeHack/bit_operation.h"
#include "Chipcode.Framework/LifeHack/light_syntax.h"
#include "Chipcode.Framework/ATMEL/ATmega128A/USART0.h" 
//#include "Chipcode.Framework/ATMEL/ATmega128A/PORTB.h" 

int main(void) 
{	
	USART0 MyUSART0(F_CPU,9600);  
	MyUSART0.EnableTransmitter();
	MyUSART0.EnableReceiver();

	// init
	SetBit(SFIOR,PUD); // ��������� ��� ������������� ��������� �� ���� ������
	ClearBit(DDRA,DDA0); //A0 �������� �� ����
	ClearBit(PORTA,PORTA0); // ��� ��� ��������� ��������
	
	while (1)
	{

		//unsigned char i = PINA; 
		//MyUSART0.TransmitOneChar('i',500);
		//MyUSART0.TransmitOneChar(i,500);
		SetBit(DDRA,DDA0); 
		SetBit(PORTA,PORTA0);
		
		//unsigned char symbol='\0'; 
		//symbol = MyUSART0.ReceiveOneChar();
		//if (symbol Not '\0')
		//{
			//MyUSART0.TransmitOneChar(symbol,10);
		//}
	}
    return 0;
}
