/*
 * AnalogComparatorAtmel.c
 *
 * Created: 20/07/2018 23:26:43
 * Author : Engineer
 */ 

#include <avr/io.h>
#define SetBit(x,y) (x|=(1<<y))
#define ClearBit(x,y) (x&=~(1<<y))
#define FlipBit(x,y) (x^=(1<<y))
#define isSetBit(x,y) (x&(1<<y))

int main(void)
{
    /* Replace with your application code */
	DDRC |= (1<<PINC0);// ��� � ����� �������� ��� �����
	DDRC |= (1<<PINC1);// ��� � ����� �������� ��� �����
	DDRC |= (1<<PINC2);// ��� � ����� �������� ��� �����
	SetBit(DDRC,PINC3); // ��� � ����� �������� ��� �����
	
	//��������� ������ ����������� ��� ����� ��� ��������
	ClearBit(DDRE,PINE2);
	ClearBit(DDRE,PINE3);
	ClearBit(PORTE,PINE2);
	ClearBit(PORTE,PINE3);
	
	ClearBit(ACSR,ACIE);  // �������� ���������� ������.����������� 
	ClearBit(ACSR,ACD);   // ����� ������� �� ������.���������� 
	SetBit(ACSR,ACIE);    // ������� ���������� ������.����������� 
	
	
    while (1) 
    {
		if (isSetBit(ACSR,ACO) )
		{
			SetBit(PORTC,PINC0);
		}
		else 
		{
			ClearBit(PORTC,PINC0);
		}
    }
}

