#ifndef _BIT_OPERATION_H_
#define _BIT_OPERATION_H_

#define SetBit(x,y) (x|=(1<<y))
#define ClearBit(x,y) (x&=~(1<<y))
#define FlipBit(x,y) (x^=(1<<y))
#define isSetBit(x,y) (x&(1<<y))
#define byte char
#define And &&
#define Or || 

#endif