/* "Chipcode.Framework/ATMEL/ATmega128A/USART0.h" */
#ifndef USART0_H
#define USART0_H
#include <avr/io.h>
#include <util/delay.h>

using namespace std; 
class USART0 
{
	private:
		unsigned long CPU_clock_Hz=8000000;
		unsigned int speed_rate_bps=9600;
		unsigned int divider=51;
		
		// to do description later
		void setDataSpeed(unsigned long CPU_clock_Hz, unsigned int speed_rate_bps)
		{
			divider = (long)CPU_clock_Hz/((long)16*(long)speed_rate_bps)-1;
			UBRR0H = (unsigned byte)(divider >> 8);
			UBRR0L = (unsigned byte) divider;
		};
		
		// to do description later
		void setDataFormat()
		{
			SetBit(UCSR0C,UCSZ00); // выбран 8 бит формат пакета
			SetBit(UCSR0C,UCSZ01); // выбран 8 бит формат пакета
		};
		
	public:
		
		//Description: speed/boud rate 9600bps at CPU clock 8MHz, data pack 8bit , stop bit 1 , parity none.
		USART0()
		{
			setDataSpeed(8000000,9600); 
			setDataFormat();
		};
		
		// to do description later
		USART0(unsigned int divider) 
		{
			UBRR0H = (unsigned byte)(divider >> 8);
			UBRR0L = (unsigned byte) divider;
			setDataFormat();
		};
		
		// to do description later
		USART0(unsigned long CPU_clock_Hz, unsigned int speed_rate_bps)
		{
			setDataSpeed(CPU_clock_Hz, speed_rate_bps);
			setDataFormat();
		};
		
		// to do description later
		void EnableTransmitter() 
		{
			SetBit(UCSR0B, TXEN0); // Разрешение работы передатчика	
		};
		
		// to do description later
		void DisableTransmitter() {
			ClearBit(UCSR0B, TXEN0); // Запрещение работы передатчика
		};
		
		// to do description later
		void EnableReceiver()
		{
			SetBit(UCSR0B,RXEN0); // разрешен прием
		};
		
		// to do description later
		void TransmitOneChar(const char OneChar, unsigned int delay_ms)
		{
			if (isSetBit(UCSR0A,UDRE0) )
			{
				UDR0 = OneChar;
			}
			_delay_ms(delay_ms);
		};
		
		// to do description later
		void TransmitOneString(const char text_message[], unsigned int delay_ms)
		{ 
			for (unsigned char index=0; text_message[index]; index++)
			{
				TransmitOneChar(text_message[index],delay_ms); 
			}
		};
		
		// to do description later
		unsigned char ReceiveOneChar()
		{
			if (isSetBit(UCSR0A,RXC0) )
			{
				return UDR0;
			}
			else 
			{
				return '\0';
			}
		};
};
#endif